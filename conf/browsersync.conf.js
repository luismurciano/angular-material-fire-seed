const conf = require('./gulp.conf');

module.exports = function () {
  return {
    server: {
      baseDir: [
        conf.paths.tmp, conf.paths.src
      ],
      routes: {
        '/bower_components': 'bower_components'
      }
    },
    // serveStatic: [{
    //   dir: [conf.paths.tmp, conf.paths.src]
    // },
    // {
    //   route: '/bower_components',
    //   dir: 'bower_components'
    // }
    // ],
    // proxy: 'http://localhost:5000',
    open: false
  };
};
