const gulp = require('gulp');
const HubRegistry = require('gulp-hub');
const browserSync = require('browser-sync');
const cache = require('gulp-cached');
const remember = require('gulp-remember');

const conf = require('./conf/gulp.conf');

// Load some files into the registry
const hub = new HubRegistry([conf.path.tasks('*.js')]);

// Tell gulp to use the tasks just loaded
gulp.registry(hub);
gulp.task('ngdocs', function () {
  const gulpDocs = require('gulp-ngdocs-components');
  return gulp.src(['src/**/*.js', '!src/**/*.spec.js'])
    .pipe(gulpDocs.process({
      scripts: [
        // 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.11/angular.js',
        // 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.11/angular-animate.js',
        'public/scripts/vendor-9af91857be.js',
        'public/maps/scripts/vendor-9af91857be.js.map',
        'https://cdn.jsdelivr.net/npm/marked@0.3.6/lib/marked.min.js',
        'https://cdn.jsdelivr.net/gh/Hypercubed/angular-marked@1.2.2/dist/angular-marked.js',
        'public/scripts/app-52d117b596.js',
        'public/maps/scripts/app-52d117b596.js.map'
      ],
      loadDefaults: {
        angular: false,
        angularAnimate: false,
        marked: false
      }
    }))
    .pipe(gulp.dest('./docs'));
});
gulp.task('connect_ngdocs', function () {
  const connect = require('gulp-connect');
  connect.server({
    root: 'docs',
    livereload: false,
    fallback: 'docs/index.html',
    port: 8083
  });
});
gulp.task('inject', gulp.series(gulp.parallel('styles', 'scripts'), 'inject'));
gulp.task('build', gulp.series('partials', gulp.parallel('inject', 'other'), 'build'));
gulp.task('test', gulp.series('scripts', 'karma:single-run'));
gulp.task('test:auto', gulp.series('watch', 'karma:auto-run'));
gulp.task('serve', gulp.series('clean', 'inject', 'watch', 'browsersync'));
gulp.task('serve:dist', gulp.series('default', 'browsersync:dist'));
gulp.task('default', gulp.series('clean', 'build'));
gulp.task('watch', watch);

function reloadBrowserSync(cb) {
  browserSync.reload();
  cb();
}

function watch(done) {
  gulp.watch([
    conf.path.src('index.html'),
    'bower.json'
  ], gulp.parallel('inject'));

  gulp.watch(conf.path.src('app/**/*.html'), gulp.series('partials', reloadBrowserSync));
  var styleWatcher = gulp.watch([
    conf.path.src('**/*.scss'),
    conf.path.src('**/*.css')
  ], gulp.series('styles'));

  styleWatcher.on('change', function (event) {
    if (event.type === 'deleted') { // if a file is deleted, forget about it
      delete cache.caches.styles[event.path];
      remember.forget('styles', event.path);
    }
  });
  var scriptWatcher = gulp.watch(conf.path.src('**/*.js'), gulp.series('inject'));
  scriptWatcher.on('change', function (event) {
    if (event.type === 'deleted') { // if a file is deleted, forget about it
      delete cache.caches.scripts[event.path];
      remember.forget('scripts', event.path);
    }
  });
  done();
}
