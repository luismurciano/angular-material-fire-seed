# Fearsome Squirrel
Fearsome-squirrel es el proyecto base sobre el que se irá desarrollando la aplicación para la gestión de candidatos y solicitudes por parte del departamento de Recursos Humanos. Este proyecto se inició con el programa de becas de Softtek en la oficina de Madrid. En este periodo los alumnos crearon ~~la mejor aplicación de todos los tiempos~~ una versión inicial de la que se intentará aprovechar lo que se pueda.

* [Tecnologías](#tecnologías)
* [Requisitos](#requisitos)
* [Instalación](#instalación)
* [Tu propio Firebase](#tu-propio-firebase)
* [Deploy en Firebase](#deploy-en-firebase)

## Tecnologías
La compleja y secreta mecánica de esta aplicación se apoya en las siguientes tecnologías para poder funcionar:

* **Front-end**:
  * [AngularJS](https://docs.angularjs.org/) - Framework javascript
  * [Angular Material](https://material.angularjs.org/) - Framework de componentes para interfaz de usuario
  * [Material Design](https://material.io/guidelines/) - Guía de estilos de Material Design
  * [Animate](https://daneden.github.io/animate.css/) - Animaciones CSS
  * [Iconos](https://material.io/icons/) - Listado de iconos disponibles
* **Back-end y base de datos**:
  * [Firebase](https://firebase.google.com/) - Plataforma de desarrollo de aplicaciones de google
        * [Lista de comandos cmd de Firebase ](https://github.com/firebase/firebase-tools#administrative-commands )  
* **Arquitectura**:
  * [Yeoman](http://yeoman.io/) - Generador de estructuras de aplicaciones
  * [Fountain](http://fountainjs.io/doc/usage/) - Generador de aplicaciones front-end
  * [Fountain Angular 1](https://github.com/FountainJS/generator-fountain-angular1) - Generador de componentes AngularJS:
        * component
        * directive
        * filter
        * service
  * [Angular Fire](https://github.com/firebase/angularfire/tree/master/docs/guide) - Generador de Firebase con AngularJS
* **Otros**:
  * [README.md](https://gitlab.com/help/user/markdown) - Guía para editar este README en Gitlab

## Requisitos
* git 2.12.2 o superior
* npm 4.6.1 o superior
* bower 1.8.0 o superior

## Instalación
1. Clona este repositorio en tu local y accede a la carpeta donde lo creaste

    ```shell
    $ git clone https://gitlab.com/luismurciano/angular-material-fire-seed.git
    ```
    ```shell
    $ cd <APP_PATH>
    ```
2. Instala las dependencias de nodeJS y bower
    ```shell
    $ npm install
    ```
    ```shell
    $ bower install
    ```
3. Arranca el servidor local para desarrollar en [localhost:3000](http://localhost:3000)
    ```shell
    $ npm run serve
    ```

## Tu propio Firebase
La aplicación ya está lista para usar, pero si quieres usar tu propia cuenta de Firebase para desarrollar también puedes hacerlo! Para ello sigue estos pasos:

1. Crea o accede a tu cuenta de [Firebase](https://firebase.google.com/)
2. Accede a la [consola de Firebase](https://console.firebase.google.com/) y crea un nuevo proyecto o usa uno que ya tengas.
3. Accede a dicho proyecto y pulsa "Añade Firebase a tu aplicación web".
4. Te saldrá un pop-up para que copies su contenido, pero en este caso no necesitamos copiar todo. Tan solo lo que hay dentro del objeto `config`:

    ```javascript
    apiKey: "<API_KEY>",
    authDomain: "<PROJECT_ID>.firebaseapp.com",
    databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
    projectId: "<PROJECT_ID>",
    storageBucket: "<BUCKET>.appspot.com",
    messagingSenderId: "<SENDER_ID>"
    ```

5. Sustituye el código copiado en el objeto `firebaseConfig` que encontrarás en tu repositorio local en el archivo `/src/app/app.module.js`

## Deploy en Firebase
Ya tienes configurada la aplicación, ahora solo queda desplegarla en el hosting de Firebase. Para ello sigue estos pasos:

1. Instala las herramientas de línea de comandos de Firebase

    ```shell
    $ npm install -g firebase-tools
    ```
2. Inicia sesión con tu cuenta de google
    ```shell
    $ firebase login
    ```
    > **Si estás trabajando detrás de un proxy no vas a poder hacer login**. Al menos no he encontrado una solución todavía. Aquí tienes algunos issues de github **sin solucionar** sobre el tema: [issue1](https://stackoverflow.com/questions/27098328/firebase-in-nodejs-with-http-proxy/28155536#28155536), [issue2](https://github.com/firebase/firebase-tools/issues/155) e [issue3](https://github.com/firebase/firebase-tools/issues/36).

3. Inicia el proyecto
    ```shell
    $ firebase init
    ```
    > Durante el proceso te preguntará que si quieres sobreescribir los archivos `firebase.json`, `database.rule.json`, `404.html`, `index.html` y el directorio de implementación. **No lo hagas**, ya que ya están configurados correctamente en el proyecto.

4. Implementa tu sitio web añadiendo tus archivos estáticos al directorio de implementación
    ```shell
    $ firebase deploy
    ```
5. Accede a la aplicación através de la URL [\<PROJECT_ID\>.firebaseapp.com](https://<PROJECT_ID>.firebaseapp.com/) o usando el comando `firebase open`
