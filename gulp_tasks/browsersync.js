const gulp = require('gulp');
const browserSync = require('browser-sync');
const spa = require('browser-sync-spa');

const browserSyncConf = require('../conf/browsersync.conf');
const browserSyncDistConf = require('../conf/browsersync-dist.conf');

// browserSync.use(spa(
//   {
//     // Only needed for angular apps
//     selector: '[ng-app]',
//
//     // Options to pass to connect-history-api-fallback.
//     // If your application already provides fallback urls (such as an existing proxy server),
//     // this value can be set to false to omit using the connect-history-api-fallback middleware entirely.
//     history: {
//       index: '/index.html'
//     }
//   }
// ));

gulp.task('browsersync', browserSyncServe);
gulp.task('browsersync:dist', browserSyncDist);

function browserSyncServe(done) {
  browserSync.init(browserSyncConf());
  done();
}

function browserSyncDist(done) {
  browserSync.init(browserSyncDistConf());
  done();
}
