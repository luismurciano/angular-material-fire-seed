angular
  .module('app')
  .config(routesConfig);
  // .run(function ($transitions) {
  //   $transitions.onStart({to: 'app.about'}, function (trans) {
  //     var auth = trans.injector().get('Auth');
  //     if (!auth.isAuthenticated()) {
  //       // User isn't authenticated. Redirect to a new Target State
  //       return trans.router.stateService.target('app.dashboard');
  //     }
  //   });
  // });

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider) {
  // $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/dashboard');

  $stateProvider
    .state('app', {
      data: {version: '0.2'},
      url: '/',
      // abstract: true,
      component: 'lmLayout',
      resolve: {
        auth: function ($log, Auth) {
          var removeCallback = Auth.$onAuthStateChanged(function (firebaseUser) {
            if (firebaseUser) {
              $log.log('Signed in as:', firebaseUser);
            } else {
              $log.log('Signed out');
            }
            removeCallback();
            return firebaseUser;
          });
          return removeCallback;
        }
      }
    })
    .state('app.login', {
      url: 'login?{redirect:json}',
      params: {
        redirect: {
          value: null,
          squash: true
        }
      },
      views: {
        'header@app': 'lmHeader',
        // targets uiview name='header' created by lmLayout
        main: {
          component: 'login'
        }
      }
    })
    .state('app.private', {
      abstract: true,
      data: {
        protected: true
      },
      views: {
        // targets uiview name='header' created by lmLayout
        'main@app': {
          component: 'lmHeader'
        }
      },
      resolve: {
        title: function ($log) {
          $log.log('resolved!');
          return 'Private Area Title';
        }
      }
    });
}
