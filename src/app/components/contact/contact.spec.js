describe('contact component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('contact', function () {
      return {
        templateUrl: 'app/contact.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<contact></contact>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
