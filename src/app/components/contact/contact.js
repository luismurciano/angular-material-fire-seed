function contactController($timeout, $q, $log, $mdToast) {
  var vm = this;
  vm.newTask = {lastModified: firebase.database.ServerValue.TIMESTAMP};
    // list of `state` value/display objects
  vm.$onInit = function () {
    vm.modifiedContact = angular.copy(vm.fireObjectContact);
    vm.states = loadAll();
    vm.selectedItem = vm.states.filter(function (state) {
      return state.value === vm.modifiedContact.state;
    }).pop();
    vm.searchText = angular.isObject(vm.selectedItem) ? vm.selectedItem.display : null;
    vm.querySearch = querySearch;
    vm.noCache = true;
    vm.log = $log.log;
  };
  vm.onSubmit = onSubmit;
  function onSubmit(form) {
    if (form.$valid) {
      $log.log(form);
      angular.forEach(form, function (valor, clave) {
        if (angular.isString(clave) && clave.length > 0 && clave.startsWith('$') === false) {
          $log.log(clave, valor);
          vm.fireObjectContact[clave] = valor.$modelValue;
        }
      });
      vm.fireObjectContact.$save();
    } else {
      $log.log('Error', form);
      $mdToast.show(
        $mdToast.simple()
        .textContent('error.code')
        .position('top right')
        .hideDelay(3000)
      );
    }
  }
    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
  function querySearch(query) {
    var results = query ? vm.states.filter(createFilterFor(query)) : vm.states;
    var deferred = $q.defer();
    $timeout(function () {
      deferred.resolve(results);
    }, Math.random() * 1000, false);
    return deferred.promise;
  }

    /**
     * Build `states` list of key/value pairs
     */
  function loadAll() {
    var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,    Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,    Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,    Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,    North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,    South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,    Wisconsin, Wyoming';

    return allStates.split(/, +/g).map(function (state) {
      return {
        value: state.toLowerCase(),
        display: state
      };
    });
  }

    /**
     * Create filter function for a query string
     */
  function createFilterFor(query) {
    var lowercaseQuery = angular.lowercase(query);

    return function (state) {
      return (state.value.indexOf(lowercaseQuery) === 0);
    };
  }
}
angular
.module('contact', ['firebase', 'ui.sidePanel', 'ui.router', 'ngMessages'])
  .config(function (hrmMenuProvider, $stateProvider) {
    // hrmMenuProvider.addMenuItem({name: 'contact', state: 'app.contacts', icon: 'info'});
    $stateProvider.state('app.contact', {
      url: 'contact/:id',
      views: {
        'header@app': 'lmHeader',
        // targets uiview name='header' created by lmLayout
        main: {
          component: 'contact'
        }
      },
      resolve: {
        title: function ($stateParams) {
          return 'Contact Title' + $stateParams.id;
        },
        fireObjectContact: function ($firebaseObject, $log, $mdToast, $stateParams) {
          var ref = firebase.database().ref();
          var contactRef = ref.child('contact').child($stateParams.id);
          // var query = messagesRef.orderByChild('lastModified').limitToLast(10);
          return $firebaseObject(contactRef).$loaded()
            .then(function (initialContact) {
              $log.log('initialContact', initialContact);
              return initialContact;
            })
            .catch(function (error) {
              $log.log('Error', error);
              if (error.code) {
                $mdToast.show(
                  $mdToast.simple()
                    .textContent(error.code)
                    .position('top right')
                    .hideDelay(3000)
                );
              }
            });
        }
      }
    });
  })
  .constant('contactModuleInfo',
  {
    name: 'contact',
    startTime: new Date()
  })
  .run(function ($log, contactModuleInfo) {
    $log.log(contactModuleInfo, moment().format('llll'));
  })
  .component('contact', {
    templateUrl: 'app/components/contact/contact.html',
    controller: contactController,
    bindings: {
      fireObjectContact: '<fireObjectContact'
    },
    require: {
      layout: '^^lmLayout'
    }
  });
