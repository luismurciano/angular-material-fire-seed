function lmhrmMenuController($timeout, $mdSidenav, $log, hrmMenu, $mdMedia) {
  var vm = this;
  vm.log = $log.log;
  vm.$mdMedia = $mdMedia;
  vm.navMenu = hrmMenu('leftSideMenu').getMenuList();
  vm.$onInit = function () {
    $log.log('hrmMenu', hrmMenu);
  };
  vm.close = function () {
    // Component lookup should always be available since we are not using `ng-if`
    $mdSidenav('left').close()
      .then(function () {
        $log.debug('close LEFT is done');
      });
  };
}

angular
  .module('ui.sidePanel', ['ngMaterial'])
  .component('hrmMenu', {
    templateUrl: 'app/components/lmSidePanel/lmSidePanel.html',
    controller: lmhrmMenuController,
    controllerAs: 'LeftPanel'
  }).provider('hrmMenu', function () {
    var menus = {};

    this.hrmMenu = menuFactory;
    function menuFactory(menuId) {
      if (angular.isUndefined(menus[menuId])) {
        menus[menuId] = [];
      }
      return new HrmMenu(menus[menuId]);
    }

    this.$get = function () {
      return function (menuId) {
        return menuFactory(menuId);
      };
    };

    // Clase Servicio
    function HrmMenu(menuItems) {
      var _menuItems = menuItems;
      this.addMenuItem = function (item) {
        return _menuItems.push(item);
      };
      this.removeMenuItem = function (item) {
        return _menuItems.filter(function (elem) {
          return elem !== item;
        });
      };
      this.getMenuList = function () {
        return angular.copy(_menuItems);
      };
    }
  });
