describe('lmSidePanel component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('lmSidePanel', function () {
      return {
        templateUrl: 'app/lmSidePanel.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<lmSidePanel></lmSidePanel>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
