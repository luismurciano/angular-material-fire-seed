/**
* @ngdoc overview
* @name app.component:lmText
*
* @description
* This component shows cards using the item binding for his own building.
*
* @param {object} item      A object with card data
* **Note:** ie<9 needs polyfill for window.getComputedStyle
 *
 * @example
   <example module="app">
     <file name="index.html">
         <lm-text data-text="'Text Component Example'"></lm-text>
     </file>
   </example>
 */
function lmTextController() {
}

angular
  .module('app')
  .component('lmText', {
    templateUrl: 'app/components/lmText/lmText.html',
    controller: lmTextController,
    bindings: {
      text: '<'
    }
  });
