describe('lmText component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('lmText', function () {
      return {
        templateUrl: 'app/lmText.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<lmText></lmText>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
