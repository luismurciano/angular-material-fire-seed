angular
  .module('about', ['firebase', 'login', 'ui.sidePanel', 'ui.router'])
  .component('about', {
    templateUrl: 'app/components/about/about.html',
    controller: aboutController,
    bindings: {
      auth: '<auth',
      infiniteItems: '<'
    },
    require: {
      layout: '^^lmLayout'
    }
  }).config(aboutConfig);

/** @ngInject */
function aboutController(Auth, $log, $mdDialog, filterFilter) {
  var vm = this;
  vm.text = 'Anonimous \'s brand new about component!';
  // var FireList = $firebaseArray(firebase.database().ref().child('tasks'));

  vm.$mdDialog = $mdDialog;
  vm.search = function (query) {
    if (angular.isString(query) && query.length > 2) {
      this.fiteredInfiniteItems = filterFilter(this.infiniteItems, query);
    } else {
      this.fiteredInfiniteItems = this.infiniteItems;
    }
  };
  vm.$onInit = function () {
    this.fiteredInfiniteItems = this.infiniteItems;
    $log.log('FireList', this.infiniteItems);
    vm.stateWatcher = Auth.$onAuthStateChanged(function (firebaseUser) {
      vm.user = firebaseUser;
      if (firebaseUser) {
        $log.log('Signed in as:', firebaseUser);
        vm.text = vm.user.displayName + '\'s brand new about component!';
      } else {
        $log.log('Signed out');
        vm.text = 'Anonimous \'s brand new about component!';
      }
    });

    $log.log('Auth', vm.auth);

    $log.log('aboutController', vm, vm.layout);
      // vm.layout.addButton('favorite');
      // vm.layout.addButton('telephone');
      // vm.layout.toggleRight();
  };
  vm.$onDestroy = function () {
    // Eliminamos el callback de modificacion de auth
    vm.stateWatcher();
  };

  vm.submitNewTask = function (form) {
    $log.log('sumbitNewTask', form);
    if (form.$valid) {
      vm.addTask(angular.extend({}, vm.newTask,
        {
          timestamp: firebase.database.ServerValue.TIMESTAMP,
          displayName: vm.user.displayName,
          photoURL: vm.user.photoURL
        }
      ));
      $mdDialog.hide('useful'); // Aqui se podria añadir algo interesante
    }
  };

  vm.addTask = function (newTask) {
    vm.infiniteItems.$add(newTask).then(function (ref) {
      var id = ref.key;
      $log.log('added record with id ' + id);
      vm.infiniteItems.$indexFor(id); // returns location in the array
    });
  };
  vm.showModalCreateTask = function () {
    $mdDialog.show({
      contentElement: '#myStaticDialog'
      // ,
      // parent: angular.element($document.body)
    }).then(function (result) {
      $log.log('showModalCreateTask success', result);
    }, function (result) {
      $log.log('showModalCreateTask reject', result);
    });
  };
}

/** @ngInject */
function aboutConfig(hrmMenuProvider, $stateProvider) {
  hrmMenuProvider.hrmMenu('leftSideMenu').addMenuItem({name: 'about', state: 'app.about', icon: 'info'});
  $stateProvider.state('app.about', {
    url: 'about',
    views: {
      'header@app': 'lmHeader',
      // targets uiview name='header' created by lmLayout
      main: {
        component: 'about'
      }
    },
    resolve: {
      title: function () {
        return 'About Title!';
      },
      infiniteItems: function ($log, $firebaseArray, $mdToast) {
        return $firebaseArray(firebase.database().ref().child('tasks')).$loaded()
        .then(function (records) {
          $log.log(records.$ref, records);
          return records;
        })
        .catch(function (error) {
          $log.log('Error', error);
          if (error.code) {
            $mdToast.show(
              $mdToast.simple()
              .textContent(error.code)
              .position('top right')
              .hideDelay(3000)
            );
          }
        });
      }

    },
    data: {
      protected: true
    }
  });
}
