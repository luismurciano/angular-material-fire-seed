describe('options component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('options', function () {
      return {
        templateUrl: 'app/options.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<options></options>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
