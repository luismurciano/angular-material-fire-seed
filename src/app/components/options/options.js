angular
  .module('options', ['ui.router', 'ui.sidePanel', 'ngMaterial'])
  .config(optionsConfig)
  .component('options', {
    templateUrl: 'app/components/options/options.html',
    controller: optionsController
  });

  /** @ngInject */
function optionsController($log, $mdTheming, theme) {
  var vm = this;
  vm.$mdTheming = $mdTheming;
  vm.theme = theme;
  vm.text = 'My brand new options component!';
  vm.themes = [];
  angular.forEach($mdTheming.THEMES, function (elem, nombre) {
    this.push(nombre);
  }, vm.themes);
  $log.log('themes', vm.themes);
}
/** @ngInject */
function optionsConfig(hrmMenuProvider, $stateProvider) {
  hrmMenuProvider.hrmMenu('leftSideMenu')
    .addMenuItem({name: 'options', state: 'app.options', icon: 'build'});
  $stateProvider.state('app.options', {
    url: 'options',
    views: {
      'header@app': 'lmHeader',
      main: {
        component: 'options'
      }
    },
    resolve: {
      title: function (theme) {
        return 'Options Title! ' + theme.currentTheme;
      }
    }
  });
}
