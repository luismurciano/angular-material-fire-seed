angular
  .module('contacts', ['firebase', 'ui.sidePanel', 'ui.router', 'contact'])
  .config(function (hrmMenuProvider, $stateProvider) {
    hrmMenuProvider.hrmMenu('leftSideMenu')
      .addMenuItem({name: 'contacts', state: 'app.contacts', icon: 'person'});
    $stateProvider.state('app.contacts', {
      url: 'contacts',
      views: {
        'header@app': 'lmHeader',
        // targets uiview name='header' created by lmLayout
        main: {
          component: 'contacts'
        }
      },
      resolve: {
        title: function () {
          return 'contacts Title!';
        },
        firelist: function ($firebaseArray, $log, $mdToast) {
          var ref = firebase.database().ref();
          var messagesRef = ref.child('contacts');
          var query = messagesRef.orderByChild('lastModified').limitToLast(10);
          return $firebaseArray(query).$loaded()
            .then(function (initialArray) {
              $log.log('initialArray', initialArray);
              return initialArray;
            })
            .catch(function (error) {
              $log.log('Error', error);
              if (error.code) {
                $mdToast.show(
                  $mdToast.simple()
                    .textContent(error.code)
                    .position('top right')
                    .hideDelay(3000)
                );
              }
            });
        }
      }
    });
  })
  .run(function ($log, $window) {
    $log.log('contacts Module Loaded', $window.moment().format('llll'));
  })
  .component('contacts', {
    templateUrl: 'app/components/contacts/contacts.html',
    controller: contactsController,
    bindings: {
      list: '<firelist'
    },
    require: {
      layout: '^^lmLayout'
    }
  }).filter('moment', function () {
    return function (date) {
      return moment(date).fromNow();
    };
  });
function contactsController($log, $firebaseArray, $mdToast, $mdDialog, $document) {
  var vm = this;
  vm.$mdDialog = $mdDialog;
  vm.text = 'My brand new contacts component!';
  vm.newTask = {lastModified: firebase.database.ServerValue.TIMESTAMP};
  vm.$onInit = function () {
    $log.log('contactsController', vm, vm.layout);
      // vm.layout.addButton('favorite');
      // vm.layout.addButton('telephone');
      // vm.layout.toggleRight();
  };
  vm.submitNewTask = function (form) {
    $log.log('sumbitNewTask', form);
    if (form.$valid) {
      vm.addTask(vm.newTask);
      $mdDialog.hide('useful');
    }
  };

  vm.addTask = function (newTask) {
    vm.list.$add(newTask).then(function (ref) {
      var id = ref.key;
      $log.log('added record with id ' + id);
      vm.list.$indexFor(id); // returns location in the array
    });
  };
  vm.showModalCreateTask = function () {
    $mdDialog.show({
      contentElement: '#myStaticDialog',
      parent: angular.element($document.body)
    }).then(function (result) {
      $log.log('showModalCreateTask success', result);
    }, function (result) {
      $log.log('showModalCreateTask reject', result);
    });
  };
}
