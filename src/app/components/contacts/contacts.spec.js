describe('contacts component', function () {
  beforeEach(module('contacts', function ($provide) {
    $provide.factory('contacts', function () {
      return {
        templateUrl: 'app/contacts.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<contacts></contacts>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
