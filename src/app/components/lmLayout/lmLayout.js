/**
 * @ngdoc directive
 * @name app.directive:rAutogrow
 * @element textarea
 * @function
 *
 * @description
 * Resize textarea automatically to the size of its text content.
 *
 * **Note:** ie<9 needs polyfill for window.getComputedStyle
 *
 * @example
   <example module="app">
     <file name="index.html">
         <textarea ng-model="text"rx-autogrow class="input-block-level"></textarea>
         <pre>{{text}}</pre>
     </file>
   </example>
 */

angular
  .module('layout', [])
  .component('lmLayout', {
    templateUrl: 'app/components/lmLayout/lmLayout.html',
    controller: 'LayoutController as app'
  })
  .component('lmHeader', {
    bindings: {
      title: '<'
    },
    require: {
      layout: '^^lmLayout'
    },
    transclude: {
      icon: '?icon'
    },
    template: '<h2 flex >{{header.title}}</h2>',
    controller: function ($log) {
      // this.title = 'Toolbar with Disabled/Enabled Icon Buttons';
      this.$onInit = function () {
        // this.layout.addPane(this);
      };
      this.$onDelete = function () {
        // this.layout.addPane(this);
      };
      this.$onChanges = function (changes) {
        $log.debug('lmHeader.$onChanges', changes);
      };
    },
    controllerAs: 'header'
  });
angular
  .module('layout')
  .run(function ($log, $window) {
    $log.log('Layout Module Loaded', $window.moment().format('llll'));
  })
  .controller('LayoutController', function (Auth, $mdMedia, $timeout, $mdSidenav, $log) {
    var vm = this;
    $log.log('Layout Controller loaded', vm);

    vm.$mdMedia = $mdMedia;
    vm.buttons = [];
    vm.leftSideMenu = {items: []};
    vm.addSideMenuButton = function (button) {
      vm.leftSideMenu.items.push(button);
    };
    vm.addButton = function (button) {
      vm.buttons.push(button);
    };
    vm.$onInit = function () {
      vm.logout = function () {
        Auth.$signOut();
      };
      Auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser) {
          $log.log('Signed in as:', firebaseUser);
        } else {
          $log.log('Signed out');
        }
        vm.user = firebaseUser;
      });
    };

    vm.toggleLeft = buildDelayedToggler('left');
    vm.toggleRight = buildToggler('right');
    vm.isOpenRight = function () {
      return $mdSidenav('right').isOpen();
    };

    /**
     * Supplies a function that will continue to operate until the
     * time is up.
     */
    function debounce(func, wait) {
      var timer;

      return function () {
        var context = vm;
        var args = Array.prototype.slice.call(arguments);
        $timeout.cancel(timer);
        timer = $timeout(function () {
          timer = undefined;
          func.apply(context, args);
        }, wait || 10);
      };
    }

    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildDelayedToggler(navID) {
      return debounce(function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug('toggle ' + navID + ' is done');
          });
      }, 200);
    }

    function buildToggler(navID) {
      return function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug('toggle ' + navID + ' is done');
          });
      };
    }
  })
  .controller('LeftController', function ($timeout, $mdSidenav, $log) {
    var vm = this;
    vm.close = function () {
      // Component lookup should always be available since we are not using `ng-if`
      $mdSidenav('left').close()
        .then(function () {
          $log.debug('close LEFT is done');
        });
    };
  })
  .controller('RightController', function ($scope, $timeout, $mdSidenav, $log) {
    var vm = this;
    vm.close = function () {
      // Component lookup should always be available since we are not using `ng-if`
      $mdSidenav('right').close()
        .then(function () {
          $log.debug('close RIGHT is done');
        });
    };
  });
