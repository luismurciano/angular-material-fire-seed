describe('lmLayout component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('lmLayout', function () {
      return {
        templateUrl: 'app/lmLayout.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<lm-layout></lm-layout>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
