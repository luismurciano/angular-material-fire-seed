angular
  .module('login', ['ui.router', 'firebase'])
  .constant('authProviders', ['facebook', 'github', 'google', 'twitter'])
  .constant('loginState', {state: 'app.login'})
  .factory('Auth', AuthFactory)
  .run(loggedOrRedirect)
  .component('login', {
    templateUrl: 'app/components/login/login.html',
    controller: loginController,
    controllerAs: 'login'
  });

/** @ngInject */
function AuthFactory($log, $firebaseAuth, $state, loginState) {
  function logOutAndGoTo(state, params) {
    return $firebaseAuth().$signOut().then(function () {
      $state.go(state, params);
    });
  }

  return angular.extend($firebaseAuth(), {
    $signOut: function (state, params) {
      return logOutAndGoTo(
        state || loginState.state,
        params || loginState.params
      );
    }
  });
}
/** @ngInject */
function loggedOrRedirect(Auth, $log, $transitions, $mdToast) {
  $transitions.onBefore({}, function (transition) {
    $log.debug('transition', $transitions, transition, transition.from(), transition.to());
  // check if the state should be protected
    if (transition.to().data && transition.to().data.protected) {
    // go to a protected state

      var solution;
      return Auth.$requireSignIn()
        .catch(function (error) {
          switch (error) {
            case 'AUTH_REQUIRED':
              solution = transition.router.stateService.go(
                'app.login',
                {
                  redirect: {
                    state: transition.to().name,
                    params: transition.router.stateService.params
                  }
                });
              break;
            default:
              $log.warn(error);
          }
          $mdToast.show(
            $mdToast.simple()
            .textContent(error)
            .position('top right')
            .hideDelay(3000)
          );
          return solution;
        });
    }
  });
}
/** @ngInject */
function loginController(Auth, authProviders, $state, $stateParams, $log) {
  var vm = this;
  this.text = 'Bienvenido a nuestra app!';
  vm.authProviders = authProviders;

  vm.$onInit = function () {
    vm.stateWatcher = Auth.$onAuthStateChanged(function (firebaseUser) {
      if (firebaseUser && $stateParams.redirect !== null) {
        $log.info('$stateParams.redirect', firebaseUser, $stateParams.redirect.state, $stateParams.redirect.params);
        $state.go($stateParams.redirect.state, $stateParams.redirect.params);
      }
    });
    $log.log('$stateParams', $stateParams);
    vm.login = function (provider) {
      Auth.$signInWithRedirect(provider).then(function (result) {
        // Never called because of page redirect
        // Instead, use $onAuthStateChanged() to detect successful authentication
        $log.log(result);
      }).catch(function (error) {
        $log.warn('Authentication failed:', error);
      });
    };
  };
  vm.$onDestroy = function () {
    // Eliminamos el callback de modificacion de auth
    vm.stateWatcher();
  };
}
