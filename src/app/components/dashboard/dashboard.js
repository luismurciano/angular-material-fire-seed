angular
  .module('dashboard', ['firebase', 'login', 'ui.sidePanel', 'ui.router'])
  .component('dashboard', {
    templateUrl: 'app/components/dashboard/dashboard.html',
    controller: dashboardController,
    bindings: {
      auth: '<auth'
    },
    require: {
      layout: '^^lmLayout'
    }
  }).config(dashboardConfig);

/** @ngInject */
function dashboardController(Auth, $log, $firebaseArray, $mdDialog, $document) {
  var vm = this;
  vm.text = 'Anonimous \'s brand new dashboard component!';
  vm.$mdDialog = $mdDialog;
  vm.$onInit = function () {
    vm.stateWatcher = Auth.$onAuthStateChanged(function (firebaseUser) {
      vm.user = firebaseUser;
      if (firebaseUser) {
        $log.log('Signed in as:', firebaseUser);
        vm.text = vm.user.displayName + '\'s brand new dashboard component!';
      } else {
        $log.log('Signed out');
        vm.text = 'Anonimous \'s brand new dashboard component!';
      }
    });

    $log.log('Auth', vm.auth);
    var ref = firebase.database().ref();
    var messagesRef = ref.child('tasks');
    var query = messagesRef.orderByChild('timestamp').limitToLast(10);

    vm.list = $firebaseArray(query);
    $log.log('dashboardController', vm, vm.layout);
      // vm.layout.addButton('favorite');
      // vm.layout.addButton('telephone');
      // vm.layout.toggleRight();
  };
  vm.$onDestroy = function () {
    // Eliminamos el callback de modificacion de auth
    vm.stateWatcher();
  };
  vm.submitNewTask = function (form) {
    $log.log('sumbitNewTask', form);
    if (form.$valid) {
      vm.addTask(vm.newTask);
      $mdDialog.hide('useful');
    }
  };

  vm.addTask = function (newTask) {
    vm.list.$add(newTask).then(function (ref) {
      var id = ref.key;
      $log.log('added record with id ' + id);
      vm.list.$indexFor(id); // returns location in the array
    });
  };
  vm.showModalCreateTask = function () {
    $mdDialog.show({
      contentElement: '#myStaticDialog',
      parent: angular.element($document.body)
    }).then(function (result) {
      $log.log('showModalCreateTask success', result);
    }, function (result) {
      $log.log('showModalCreateTask reject', result);
    });
  };
}

/** @ngInject */
function dashboardConfig(hrmMenuProvider, $stateProvider) {
  hrmMenuProvider.hrmMenu('leftSideMenu').addMenuItem({name: 'dashboard', state: 'app.private.dashboard', icon: 'dashboard'});
  $stateProvider.state('app.private.dashboard', {
    url: 'dashboard',
    views: {
      'header@app': 'lmHeader',
      // targets uiview name='header' created by lmLayout
      main: {
        component: 'dashboard'
      }
    },
    resolve: {
      title: function () {
        return 'dashboard Title!';
      }
    }

    // onEnter: ['loggedOrRedirect', '$log', function (loggedOrRedirect, $log) {
    //   $log.log('loggedOrRedirect', loggedOrRedirect);
    //   // return loggedOrRedirect;
    // }]
  });
}
