angular
  .module('app')
  .factory('firebase', function ($window) {
    return $window.firebase;
  })
  .constant('theme', {currentTheme: 'dark-yellow'})
  .config(function ($mdIconProvider, $mdThemingProvider) {
    $mdIconProvider.defaultFontSet('material-icons');

    $mdThemingProvider.theme('default');
    $mdThemingProvider.theme('dark-grey').backgroundPalette('grey').dark();
    $mdThemingProvider.theme('dark-orange').backgroundPalette('orange').dark();
    $mdThemingProvider.theme('dark-purple').backgroundPalette('deep-purple').dark();
    $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark();
    $mdThemingProvider.theme('dark-yellow').backgroundPalette('grey').primaryPalette('yellow').dark();

    // $mdThemingProvider.setDefaultTheme(theme.currentTheme);
  })
  .config(function ($mdProgressCircularProvider) {
    // Example of changing the default progress options.
    $mdProgressCircularProvider.configure({
      progressSize: 300,
      strokeWidth: 4,
      duration: 800,
      durationIndeterminate: 6543
    });
  })
  .controller('AppController', AppController)
  .component('app', {
    templateUrl: 'app/app.html',
    controller: 'AppController as app'
  });
/** @ngInject */
function AppController($log, theme) {
  var vm = this;
  vm.theme = theme;
  $log.log('App controller loaded', vm);
  vm.text = 'Loading asserts, please wait.';
}
