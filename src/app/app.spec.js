describe('app component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('app', function () {
      return {
        templateUrl: 'app/app.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<app></app>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
