angular
  .module('app',
  [
    'login',
    'ui.router',
    'ngMaterial',
    'firebase',
    'ngAnimate',
    'layout',
    'dashboard',
    'contacts',
    'options',
    'about'
  ])

  .constant('firebaseConfig', {
    apiKey: 'AIzaSyAEGPIkoi7d87cSsrE6-M1UDrvdft3Uwyw',
    authDomain: 'fearsome-squirrel.firebaseapp.com',
    databaseURL: 'https://fearsome-squirrel.firebaseio.com',
    projectId: 'fearsome-squirrel',
    storageBucket: 'fearsome-squirrel.appspot.com',
    messagingSenderId: '395472154975'
  })
  .config(configFunction)
  .run(function ($rootScope, $log, $injector, $mdTheming) {
    $log.info('Modules: ', $injector.modules);
    $log.info('Installed Themes: ', $mdTheming.THEMES);
  });
/** @ngInject */
function configFunction(firebaseConfig, $compileProvider) {
  var $injector = angular.injector(['ng']);
  var $log = $injector.get('$log');
  var $window = $injector.get('$window');
  $compileProvider.debugInfoEnabled(false);
  $log.log('Configuring Firebase', firebaseConfig, $window.firebase.initializeApp(firebaseConfig));
}
