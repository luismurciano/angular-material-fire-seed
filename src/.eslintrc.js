module.exports = {
  extends: [
    'angular'
  ],
  globals: {
    'firebase': true,
    'moment': true
  },
  rules: {
    'angular/no-service-method': 0
  }
}
